function showHidden(id) {
    const ELEMENT = document.getElementById(id);

    if (ELEMENT.classList.contains('h-0')) {
        ELEMENT.classList.remove('h-0')
        ELEMENT.classList.add('mt-5')
        ELEMENT.classList.add('h-auto');
    } else {
        ELEMENT.classList.remove('h-auto');
        ELEMENT.classList.remove('mt-5')
        ELEMENT.classList.add('h-0');
    }
}