var db = require('./db.js');

module.exports = {
    read: function (id, callback) {
        db.query("SELECT * FROM FichePoste WHERE ID = ?", id, function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readAll: function (callback) {
        db.query("SELECT * FROM FichePoste", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    create: function (titre, statut, type, respHier, lieu, rythme, salaireMin, salaireMax, description, sirene, numOffre, callback) {
        var values = [titre, statut, type, respHier, lieu, rythme, salaireMin, salaireMax, description, sirene, numOffre];
        db.query("INSERT INTO FichePoste (Titre, Statut, Type, RespHier, Lieu, Rythme, Salaire_min, Salaire_max, Description, SIREN, NumOffre) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values, function (err, result) {
            if (err) throw err;
            callback(result.insertId); // Return the ID of the inserted job position
        });
    }
};
