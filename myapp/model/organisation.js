var db = require('./db.js');

module.exports = {
    read: function (siren, callback) {
        db.query("SELECT * FROM Organisation WHERE SIREN = ?", siren, function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readAll: function (callback) {
        db.query("SELECT * FROM Organisation", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    create: function (siren, nom, type, siegeSocial, callback) {
        var values = [siren, nom, type, siegeSocial];
        db.query("INSERT INTO Organisation (SIREN, Nom, Type, SiegeSocial) VALUES (?, ?, ?, ?)", values, function (err, result) {
            if (err) throw err;
            callback(result.insertId); // Return the ID of the inserted organisation
        });
    }
};
