var db = require('./connection.js');

module.exports = {
    read: function (email, callback) {
        db.query("SELECT * FROM Utilisateur WHERE Email = ?", email, function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readAll: function (callback) {
        return new Promise((resolve, reject) => {
            db.query("SELECT * FROM Utilisateur", function (err, results) {
                if (err) reject(err);
                resolve(results);
            });

        });
        
    },
    areValid: function (email, password, callback) {
        db.query("SELECT Mdp FROM Utilisateur WHERE Email = ?", email, function (err, rows) {
            if (err) throw err;
            if (rows.length === 1 && rows[0].Mdp === password) {
                callback(true);
            } else {
                callback(false);
            }
        });
    },
    create: function (nom, prenom, email, tel, mdp, statut, dateCreation, organisation, callback) {
        var values = [nom, prenom, email, tel, mdp, statut, dateCreation, organisation];
        db.query("INSERT INTO Utilisateur (Nom, Prenom, Email, Tel, Mdp, Statut, DateCreation, Organisation) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", values, function (err, result) {
            if (err) throw err;
            callback(result.insertId); // Return the ID of the inserted user
        });
    }
};
