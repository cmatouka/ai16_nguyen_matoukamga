var db = require('./db.js');

module.exports = {
    read: function (numOffre, callback) {
        db.query("SELECT * FROM OffreEmploi WHERE NumOffre = ?", numOffre, function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readAll: function (callback) {
        db.query("SELECT * FROM OffreEmploi", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    create: function (titre, pieces, nbPieces, description, salaire, datePublication, dateExpiration, etat, callback) {
        var values = [titre, pieces, nbPieces, description, salaire, datePublication, dateExpiration, etat];
        db.query("INSERT INTO OffreEmploi (Titre, Pieces, NbPieces, Description, Salaire, DatePublication, DateExpiration, Etat) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", values, function (err, result) {
            if (err) throw err;
            callback(result.insertId); // Return the ID of the inserted job offer
        });
    }
};
