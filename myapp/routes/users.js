var express = require('express');
var router = express.Router();
var utilisateurs = require('../model/utilisateurs');

/* GET user creation form. */
router.get('/users/new', function(req, res, next) {
    res.render('createUser', { title: 'Créer un utilisateur' });
});

/* GET users listing. */
router.get('/', async function(req, res, next) {
    try {
        const result = await utilisateurs.readAll();
        //console.log(result);
        res.render('usersList', { title: 'Liste des utilisateurs', utilisateurs: result });
    } catch (err) {
        next(err);  
    }
});

router.post('/users', async function(req, res, next) {
    const user_fname = req.body.fname;
    const user_lname = req.body.lname;
    const user_email = req.body.email;
    const user_tel = req.body.tel;
    const user_mdp = req.body.mdp;
    const user_statut = req.body.statut;
    const user_dateCreation = req.body.dateCreation;
    const user_organisation = req.body.organisation;

    
    try {
        const newUser = await utilisateurs.create({ fname: user_fname, lname: user_lname, email: user_email, tel :user_tel, mdp: user_mdp, statut: user_statut, dateCreation: user_dateCreation, organisation: user_organisation });  // Create new user
        res.render('createUser', { title: 'Créer un utilisateur', utilisateur: newUser });
    } catch (err) {
        next(err);  
    }
});

module.exports = router;
